/*
* The ASCII protocol is a simpler, human readable alternative to the main native
* protocol.
* In the future this protocol might be extended to support selected GCode commands.
* For a list of supported commands see doc/ascii-protocol.md
*/

/* Includes ------------------------------------------------------------------*/

#include "odrive_main.h"
#include "communication.h"
#include "ascii_protocol.hpp"
#include <utils.hpp>
#include <fibre/cpp_utils.hpp>

#include "autogen/type_info.hpp"
#include "communication/interface_can.hpp"

using namespace fibre;

/* Private macros ------------------------------------------------------------*/
/* Private typedef -----------------------------------------------------------*/
/* Global constant data ------------------------------------------------------*/
/* Global variables ----------------------------------------------------------*/
/* Private constant data -----------------------------------------------------*/

#define TO_STR_INNER(s) #s
#define TO_STR(s) TO_STR_INNER(s)

/* Private variables ---------------------------------------------------------*/

#if HW_VERSION_MAJOR == 3
static Introspectable root_obj = ODrive3TypeInfo<ODrive>::make_introspectable(odrv);
#elif HW_VERSION_MAJOR == 4
static Introspectable root_obj = ODrive4TypeInfo<ODrive>::make_introspectable(odrv);
#endif

/* Private function prototypes -----------------------------------------------*/

/* Function implementations --------------------------------------------------*/

// @brief Sends a line on the specified output.
template<typename ... TArgs>
void AsciiProtocol::respond(bool include_checksum, const char * fmt, TArgs&& ... args) {
    size_t len = snprintf(tx_buf_, sizeof(tx_buf_), fmt, std::forward<TArgs>(args)...);

    // Silently truncate the output if it's too long for the buffer.
    len = std::min(len, sizeof(tx_buf_));

    if (include_checksum) {
        uint8_t checksum = 0;
        for (size_t i = 0; i < len; ++i)
            checksum ^= tx_buf_[i];
        len += snprintf(tx_buf_ + len, sizeof(tx_buf_) - len, "*%u", checksum);
    } 
    len += snprintf(tx_buf_ + len, sizeof(tx_buf_) - len, "\r\n");

    // Silently truncate the output if it's too long for the buffer.
    len = std::min(len, sizeof(tx_buf_));

    tx_end_ = (const uint8_t*)tx_buf_ + len;
    tx_channel_->start_write({(const uint8_t*)tx_buf_, tx_end_}, &tx_handle_, MEMBER_CB(this, on_write_finished));
}


// @brief Executes an ASCII protocol command
// @param buffer buffer of ASCII encoded characters
// @param len size of the buffer
void AsciiProtocol::process_line(cbufptr_t buffer) {
    static_assert(sizeof(char) == sizeof(uint8_t));
    
    // scan line to find beginning of checksum and prune comment
    uint8_t checksum = 0;
    size_t checksum_start = SIZE_MAX;
    for (size_t i = 0; i < buffer.size(); ++i) {
        if (buffer.begin()[i] == ';') { // ';' is the comment start char
            buffer = buffer.take(i);
            break;
        }
        if (checksum_start > i) {
            if (buffer[i] == '*') {
                checksum_start = i + 1;
            } else {
                checksum ^= buffer[i];
            }
        }
    }

    // copy everything into a local buffer so we can insert null-termination
    char cmd[MAX_LINE_LENGTH + 1];
    size_t len = std::min(buffer.size(), MAX_LINE_LENGTH);
    memcpy(cmd, buffer.begin(), len);
    cmd[len] = 0; // null-terminate

    // optional checksum validation
    bool use_checksum = (checksum_start < len);

    if (use_checksum) {
        unsigned int received_checksum;
        int numscan = sscanf(&cmd[checksum_start], "%u", &received_checksum);
        if ((numscan < 1) || (received_checksum != checksum))
            return;
        len = checksum_start - 1; // prune checksum and asterisk
        cmd[len] = 0; // null-terminate
    }


    // check incoming packet type
    switch(cmd[0]) {
        case 'a': cmd_set_abs_position(cmd, use_checksum);            break;  // absolute position
        case 'P': cmd_set_position_PID(cmd, use_checksum);            break;  // position pid
        case 'p': cmd_set_position(cmd, use_checksum);                break;  // position control
        case 'q': cmd_set_position_wl(cmd, use_checksum);             break;  // position control with limits
        case 'v': cmd_set_velocity(cmd, use_checksum);                break;  // velocity control
        case 'T': cmd_set_torque_PD(cmd, use_checksum);              break;  // torque pid
        case 'c': cmd_set_torque(cmd, use_checksum);                  break;  // current control
        case 'b': cmd_set_moment(cmd, use_checksum);                  break;  // torque control 
        case 'd': cmd_set_weights(cmd, use_checksum);                 break;  // fuzzy weights
        case 'F': cmd_set_fuzzy(cmd, use_checksum);                  break;  // set fuzzy weights as well as setpoints
        case 't': cmd_set_trapezoid_trajectory(cmd, use_checksum);    break;  // trapezoidal trajectory
        case 'f': cmd_get_feedback(cmd, use_checksum);                break;  // feedback
        case 'm': cmd_get_march_data(cmd, use_checksum);              break;  // MARCH data
        case 'x': cmd_get_all_errors(cmd, use_checksum);              break;  // All errors
        case 'h': cmd_help(cmd, use_checksum);                        break;  // Help
        case 'i': cmd_info_dump(cmd, use_checksum);                   break;  // Dump device info
        case 's': cmd_system_ctrl(cmd, use_checksum);                 break;  // System
        case 'r': cmd_read_property(cmd,  use_checksum);              break;  // read property
        case 'w': cmd_write_property(cmd, use_checksum);              break;  // write property
        case 'u': cmd_update_axis_wdg(cmd, use_checksum);             break;  // Update axis watchdog. 
        case 'e': cmd_encoder(cmd, use_checksum);                     break;  // Encoder commands
        case '+':
        case '-': cmd_torque_sensor(cmd, use_checksum);               break;  // Torque response
        default : cmd_unknown(nullptr, use_checksum);                 break;
    }
}

// @brief Sets the encoder linear count
// @param pStr buffer of ASCII encoded values
// @param response_channel reference to the stream to respond on
// @param use_checksum bool to indicate whether a checksum is required on response
void AsciiProtocol::cmd_set_abs_position(char * pStr, bool use_checksum) {
    unsigned motor_number;
    float abs_position;

    if (sscanf(pStr, "a %u %f", &motor_number, &abs_position) < 2) {
        respond(use_checksum, "invalid command format");
    } else if (motor_number >= AXIS_COUNT) {
        respond(use_checksum, "invalid motor %u", motor_number);
    } else {
        Axis& axis = axes[motor_number];
        axis.controller_.config_.control_mode = Controller::CONTROL_MODE_FUZZY_CONTROL;
        axis.encoder_.pos_abs_= static_cast<int32_t>(abs_position);
        axis.watchdog_feed();

        // We send back a respond for debugging purpose
        respond(use_checksum, "%ld ", axis.encoder_.pos_abs_);
    }
}

// @brief Sets the PID values of position control
// @param pStr buffer of ASCII encoded values
// @param response_channel reference to the stream to respond on
// @param use_checksum bool to indicate whether a checksum is required on response
void AsciiProtocol::cmd_set_position_PID(char * pStr, bool use_checksum) {
    unsigned motor_number;
    float positionP;
    float positionI;
    float positionD;


    if (sscanf(pStr, "P %u %f %f %f", &motor_number, &positionP, &positionI, &positionD) < 2) {
        respond(use_checksum, "invalid command format");
    } else if (motor_number >= AXIS_COUNT) {
        respond(use_checksum, "invalid motor %u", motor_number);
    } else {
        Axis& axis = axes[motor_number];
        axis.controller_.config_.pos_gain = positionP;
        axis.controller_.config_.vel_integrator_gain = positionI;
        axis.controller_.config_.vel_gain = positionD;
        axis.watchdog_feed();

        // We send back a respond for debugging purpose
        respond(use_checksum, "%f %f %f", axis.controller_.config_.pos_gain, axis.controller_.config_.vel_integrator_gain, axis.controller_.config_.vel_gain);
    }
}

// @brief Sets the pid values of torque control
// @param pStr buffer of ASCII encoded values
// @param response_channel reference to the stream to respond on
// @param use_checksum bool to indicate whether a checksum is required on response
void AsciiProtocol::cmd_set_torque_PD(char * pStr, bool use_checksum) {
    unsigned motor_number;
    float torqueP;
    float torqueD;


    if (sscanf(pStr, "T %u %f %f %f", &motor_number, &torqueP, &torqueD) < 2) {
        respond(use_checksum, "invalid command format");
    } else if (motor_number >= AXIS_COUNT) {
        respond(use_checksum, "invalid motor %u", motor_number);
    } else {
        Axis& axis = axes[motor_number];
        axis.controller_.config_.moment_gain = torqueP;
        axis.controller_.config_.moment_deriv_gain = torqueD;
        axis.watchdog_feed();

        // We send back a respond for debugging purpose
        respond(use_checksum, "%f %f %f", axis.controller_.config_.moment_gain, axis.controller_.config_.moment_deriv_gain);
    }
}

// @brief Executes the set position command
// @param pStr buffer of ASCII encoded values
// @param response_channel reference to the stream to respond on
// @param use_checksum bool to indicate whether a checksum is required on response
void AsciiProtocol::cmd_set_position(char * pStr, bool use_checksum) {
    unsigned motor_number;
    float pos_setpoint, vel_feed_forward, torque_feed_forward;

    int numscan = sscanf(pStr, "p %u %f %f %f", &motor_number, &pos_setpoint, &vel_feed_forward, &torque_feed_forward);
    if (numscan < 2) {
        respond(use_checksum, "invalid command format");
    } else if (motor_number >= AXIS_COUNT) {
        respond(use_checksum, "invalid motor %u", motor_number);
    } else {
        Axis& axis = axes[motor_number];
        axis.controller_.config_.control_mode == Controller::CONTROL_MODE_FUZZY_CONTROL;
        axis.controller_.input_pos_ = pos_setpoint;
        if (numscan >= 3)
            axis.controller_.input_vel_ = vel_feed_forward;
        if (numscan >= 4)
            axis.controller_.input_torque_ = torque_feed_forward;
        axis.controller_.input_pos_updated();
        axis.watchdog_feed();
    }
}

// @brief Executes the set moment command
// @param pStr buffer of ASCII encoded values
// @param response_channel reference to the stream to respond on
// @param use_checksum bool to indicate whether a checksum is required on response
void AsciiProtocol::cmd_set_moment(char * pStr, bool use_checksum) {
    unsigned motor_number;
    float moment_setpoint, vel_feed_forward, torque_feed_forward;

    int numscan = sscanf(pStr, "b %u %f %f %f", &motor_number, &moment_setpoint, &vel_feed_forward, &torque_feed_forward);
    if (numscan < 2) {
        respond(use_checksum, "invalid command format");
    } else if (motor_number >= AXIS_COUNT) {
        respond(use_checksum, "invalid motor %u", motor_number);
    } else {
        Axis& axis = axes[motor_number];
        axis.controller_.config_.control_mode = Controller::CONTROL_MODE_FUZZY_CONTROL;
        axis.controller_.input_moment_ = moment_setpoint;
        if (numscan >= 3)
            axis.controller_.input_vel_ = vel_feed_forward;
        if (numscan >= 4)
            axis.controller_.input_torque_ = torque_feed_forward;
        axis.controller_.input_moment_updated();
        axis.watchdog_feed();
    }
}

// @brief Executes the set weights command
// @param pStr buffer of ASCII encoded values
// @param response_channel reference to the stream to respond on
// @param use_checksum bool to indicate whether a checksum is required on response
void AsciiProtocol::cmd_set_weights(char * pStr, bool use_checksum) {
    unsigned motor_number;
    float moment_weight, position_weight;

    int numscan = sscanf(pStr, "d %u %f %f", &motor_number, &moment_weight, &position_weight);
    if (numscan < 2) {
        respond(use_checksum, "invalid command format");
    } else if (motor_number >= AXIS_COUNT) {
        respond(use_checksum, "invalid motor %u", motor_number);
    } else {
        Axis& axis = axes[motor_number];
        axis.controller_.config_.control_mode = Controller::CONTROL_MODE_FUZZY_CONTROL;
        axis.controller_.weight_moment_ = moment_weight;
        axis.controller_.weight_position_ = position_weight;
        axis.watchdog_feed();
    }
}

// @brief Executes the set fuzzy command
// @param pStr buffer of ASCII encoded values
// @param response_channel reference to the stream to respond on
// @param use_checksum bool to indicate whether a checksum is required on response
void AsciiProtocol::cmd_set_fuzzy(char * pStr, bool use_checksum) {
    unsigned motor_number;
    float pos_setpoint, moment_setpoint, position_weight, moment_weight, vel_feed_forward, torque_feed_forward;

    int numscan = sscanf(pStr, "F %u %f %f %f %f %f %f", &motor_number, &pos_setpoint, &moment_setpoint, &position_weight, &moment_weight, &vel_feed_forward, &torque_feed_forward);
    if (numscan < 2) {
        respond(use_checksum, "invalid command format");
    } else if (motor_number >= AXIS_COUNT) {
        respond(use_checksum, "invalid motor %u", motor_number);
    } else {
        Axis& axis = axes[motor_number];
        axis.controller_.config_.control_mode = Controller::CONTROL_MODE_FUZZY_CONTROL;
        axis.controller_.input_pos_ = pos_setpoint;
        axis.controller_.input_moment_ = moment_setpoint;
        axis.controller_.weight_position_ = position_weight;
        axis.controller_.weight_moment_ = moment_weight;
        axis.controller_.input_pos_updated();
        axis.controller_.input_moment_updated();

        if (numscan >= 5)
            axis.controller_.input_vel_ = vel_feed_forward;
        if (numscan >= 6)
            axis.controller_.input_torque_ = torque_feed_forward;

        axis.watchdog_feed();

    }

   
}

// @brief Executes the set position with current and velocity limit command
// @param pStr buffer of ASCII encoded values
// @param response_channel reference to the stream to respond on
// @param use_checksum bool to indicate whether a checksum is required on response
void AsciiProtocol::cmd_set_position_wl(char * pStr, bool use_checksum) {
    unsigned motor_number;
    float pos_setpoint, vel_limit, torque_lim;

    int numscan = sscanf(pStr, "q %u %f %f %f", &motor_number, &pos_setpoint, &vel_limit, &torque_lim);
    if (numscan < 2) {
        respond(use_checksum, "invalid command format");
    } else if (motor_number >= AXIS_COUNT) {
        respond(use_checksum, "invalid motor %u", motor_number);
    } else {
        Axis& axis = axes[motor_number];
        axis.controller_.config_.control_mode = Controller::CONTROL_MODE_FUZZY_CONTROL;
        axis.controller_.input_pos_ = pos_setpoint;
        if (numscan >= 3)
            axis.controller_.config_.vel_limit = vel_limit;
        if (numscan >= 4)
            axis.motor_.config_.torque_lim = torque_lim;
        axis.controller_.input_pos_updated();
        axis.watchdog_feed();
    }
}

// @brief Executes the set velocity command
// @param pStr buffer of ASCII encoded values
// @param response_channel reference to the stream to respond on
// @param use_checksum bool to indicate whether a checksum is required on response
void AsciiProtocol::cmd_set_velocity(char * pStr, bool use_checksum) {
    unsigned motor_number;
    float vel_setpoint, torque_feed_forward;
    int numscan = sscanf(pStr, "v %u %f %f", &motor_number, &vel_setpoint, &torque_feed_forward);
    if (numscan < 2) {
        respond(use_checksum, "invalid command format");
    } else if (motor_number >= AXIS_COUNT) {
        respond(use_checksum, "invalid motor %u", motor_number);
    } else {
        Axis& axis = axes[motor_number];
        axis.controller_.config_.control_mode = Controller::CONTROL_MODE_VELOCITY_CONTROL;
        axis.controller_.input_vel_ = vel_setpoint;
        if (numscan >= 3)
            axis.controller_.input_torque_ = torque_feed_forward;
        axis.watchdog_feed();
    }
}

// @brief Executes the set torque control command
// @param pStr buffer of ASCII encoded values
// @param response_channel reference to the stream to respond on
// @param use_checksum bool to indicate whether a checksum is required on response
void AsciiProtocol::cmd_set_torque(char * pStr, bool use_checksum) {
    unsigned motor_number;
    float torque_setpoint;

    if (sscanf(pStr, "c %u %f", &motor_number, &torque_setpoint) < 2) {
        respond(use_checksum, "invalid command format");
    } else if (motor_number >= AXIS_COUNT) {
        respond(use_checksum, "invalid motor %u", motor_number);
    } else {
        Axis& axis = axes[motor_number];
        // axis.controller_.config_.control_mode = Controller::CONTROL_MODE_TORQUE_CONTROL;
        axis.controller_.input_torque_ = torque_setpoint;
        // axis.watchdog_feed();
    }
}

// @brief Sets the encoder linear count
// @param pStr buffer of ASCII encoded values
// @param response_channel reference to the stream to respond on
// @param use_checksum bool to indicate whether a checksum is required on response
void AsciiProtocol::cmd_encoder(char * pStr, bool use_checksum) {
    if (pStr[1] == 's') {
        pStr += 2; // Substring two characters to the right (ok because we have guaranteed null termination after all chars)

        unsigned motor_number;
        int encoder_count;

        if (sscanf(pStr, "l %u %i", &motor_number, &encoder_count) < 2) {
            respond(use_checksum, "invalid command format");
        } else if (motor_number >= AXIS_COUNT) {
            respond(use_checksum, "invalid motor %u", motor_number);
        } else {
            Axis& axis = axes[motor_number];
            axis.encoder_.set_linear_count(encoder_count);
            axis.watchdog_feed();
            respond(use_checksum, "encoder set to %u", encoder_count);
        }
    } else {
        respond(use_checksum, "invalid command format");
    }
}

// @brief Executes the set trapezoid trajectory command
// @param pStr buffer of ASCII encoded values
// @param response_channel reference to the stream to respond on
// @param use_checksum bool to indicate whether a checksum is required on response
void AsciiProtocol::cmd_set_trapezoid_trajectory(char* pStr, bool use_checksum) {
    unsigned motor_number;
    float goal_point;

    if (sscanf(pStr, "t %u %f", &motor_number, &goal_point) < 2) {
        respond(use_checksum, "invalid command format");
    } else if (motor_number >= AXIS_COUNT) {
        respond(use_checksum, "invalid motor %u", motor_number);
    } else {
        Axis& axis = axes[motor_number];
        axis.controller_.config_.input_mode = Controller::INPUT_MODE_TRAP_TRAJ;
        axis.controller_.config_.control_mode = Controller::CONTROL_MODE_FUZZY_CONTROL;
        axis.controller_.input_pos_ = goal_point;
        axis.controller_.input_pos_updated();
        axis.watchdog_feed();
    }
}

// @brief Executes the get position and velocity feedback command
// @param pStr buffer of ASCII encoded values
// @param response_channel reference to the stream to respond on
// @param use_checksum bool to indicate whether a checksum is required on response
void AsciiProtocol::cmd_get_feedback(char * pStr, bool use_checksum) {
    unsigned motor_number;

    if (sscanf(pStr, "f %u", &motor_number) < 1) {
        respond(use_checksum, "invalid command format");
    } else if (motor_number >= AXIS_COUNT) {
        respond(use_checksum, "invalid motor %u", motor_number);
    } else {
        Axis& axis = axes[motor_number];
        respond(use_checksum, "%f %f",
                (double)axis.encoder_.pos_estimate_.any().value_or(0.0f),
                (double)axis.encoder_.vel_estimate_.any().value_or(0.0f));
    }
}

// @brief Executes the get march data command
// @param pStr buffer of ASCII encoded values
// @param response_channel reference to the stream to respond on
// @param use_checksum bool to indicate whether a checksum is required on response
void AsciiProtocol::cmd_get_march_data(char * pStr, bool use_checksum) {
    unsigned motor_number;
    // float measured_torque;

    if (sscanf(pStr, "m %u", &motor_number) < 1) {
        Axis& axis0 = axes[0];
        Axis& axis1 = axes[1];

        int axis0_any_error = odrv.error_ | axis0.error_ | axis0.motor_.error_ | axis0.encoder_.error_ | axis0.torque_sensor_.error_ | axis0.controller_.error_;
        int axis1_any_error = odrv.error_ | axis1.error_ | axis1.motor_.error_ | axis1.encoder_.error_ | axis1.torque_sensor_.error_ | axis1.controller_.error_;
        respond(use_checksum, "%u %d %d %f %f %f %f %u %d %d %f %f %f %f",
                (unsigned int)axis0.current_state_,
                (int)axis0_any_error,
                (int)axis0.encoder_.shadow_count_,
                (float)axis0.encoder_.vel_estimate_.any().value_or(0.0f),
                (float)axis0.motor_.current_control_.Iq_measured_,
                (float)axis0.motor_.fet_thermistor_.temperature_,
                (float)axis0.torque_sensor_.moment_estimate_.any().value_or(0.0f),
                (unsigned int)axis1.current_state_,
                (int)axis1_any_error,
                (int)axis1.encoder_.shadow_count_,
                (float)axis1.encoder_.vel_estimate_.any().value_or(0.0f),
                (float)axis1.motor_.current_control_.Iq_measured_,
                (float)axis1.motor_.fet_thermistor_.temperature_,
                (float)axis1.torque_sensor_.moment_estimate_.any().value_or(0.0f));
    } else if (motor_number >= AXIS_COUNT) {
        respond(use_checksum, "invalid motor %u", motor_number);
    } else {
        Axis& axis = axes[motor_number];
        unsigned int axis_any_error =   (unsigned int)axis.error_ | (unsigned int)axis.motor_.error_ | 
                                        (unsigned int)axis.encoder_.error_ | (unsigned int)axis.torque_sensor_.error_ | (unsigned int)axis.controller_.error_;
        // axis.torque_sensor_.raw_moment = measured_torque; // Temporary until torque sensors can be connected to the ODrive directly
        respond(use_checksum, "%u %u %d %f %f %f %f",
                (unsigned int)axis.current_state_,
                (unsigned int)axis_any_error,
                (int)axis.encoder_.shadow_count_,
                (double)axis.encoder_.vel_estimate_.any().value_or(0.0f),
                (double)axis.motor_.current_control_.Iq_measured_,
                (double)axis.motor_.fet_thermistor_.temperature_,
                (double)axis.controller_.integral_,    // reset controller
                (double)axis.torque_sensor_.moment_estimate_.any().value_or(0.0f));
    }
}

// @brief Executes the get all errors command
// @param pStr buffer of ASCII encoded values
// @param response_channel reference to the stream to respond on
// @param use_checksum bool to indicate whether a checksum is required on response
void AsciiProtocol::cmd_get_all_errors(char * pStr, bool use_checksum) {
    unsigned motor_number;

    if (sscanf(pStr, "x %u", &motor_number) < 1) {
        Axis& axis0 = axes[0];
        Axis& axis1 = axes[1];
        respond(use_checksum, "%u %u %u %u %u %u %u %u %u %u %u",
                (unsigned int)odrv.error_,
                (unsigned int)axis0.error_,
                (unsigned int)axis0.motor_.error_,
                (unsigned int)axis0.encoder_.error_,
                (unsigned int)axis0.torque_sensor_.error_,
                (unsigned int)axis0.controller_.error_,
                (unsigned int)axis1.error_,
                (unsigned int)axis1.motor_.error_,
                (unsigned int)axis1.encoder_.error_,
                (unsigned int)axis1.torque_sensor_.error_,
                (unsigned int)axis1.controller_.error_);
    } else if (motor_number >= AXIS_COUNT) {
        respond(use_checksum, "invalid motor %u", motor_number);
    } else {
        Axis& axis = axes[motor_number];
        respond(use_checksum, "%u %u %u %u %u",
                (unsigned int)odrv.error_,
                (unsigned int)axis.error_,
                (unsigned int)axis.motor_.error_,
                (unsigned int)axis.encoder_.error_,
                (unsigned int)axis.torque_sensor_.error_,
                (unsigned int)axis.controller_.error_);
    }
}


// @brief Shows help text
// @param pStr buffer of ASCII encoded values
// @param response_channel reference to the stream to respond on
// @param use_checksum bool to indicate whether a checksum is required on response
void AsciiProtocol::cmd_help(char * pStr, bool use_checksum) {
    (void)pStr;
    respond(use_checksum, "Please see documentation for more details");
    respond(use_checksum, "");
    respond(use_checksum, "Available commands syntax reference:");
    respond(use_checksum, "Position: q axis pos vel-lim I-lim");
    respond(use_checksum, "Position: p axis pos vel-ff I-ff");
    respond(use_checksum, "Velocity: v axis vel I-ff");
    respond(use_checksum, "Torque: c axis T");
    respond(use_checksum, "");
    respond(use_checksum, "Properties start at odrive root, such as axis0.requested_state");
    respond(use_checksum, "Read: r property");
    respond(use_checksum, "Write: w property value");
    respond(use_checksum, "");
    respond(use_checksum, "Save config: ss");
    respond(use_checksum, "Erase config: se");
    respond(use_checksum, "Reboot: sr");
}

// @brief Gets the hardware, firmware and serial details
// @param pStr buffer of ASCII encoded values
// @param response_channel reference to the stream to respond on
// @param use_checksum bool to indicate whether a checksum is required on response
void AsciiProtocol::cmd_info_dump(char * pStr, bool use_checksum) {
    // respond(use_checksum, "Signature: %#x", STM_ID_GetSignature());
    // respond(use_checksum, "Revision: %#x", STM_ID_GetRevision());
    // respond(use_checksum, "Flash Size: %#x KiB", STM_ID_GetFlashSize());
    respond(use_checksum, "Hardware version: %d.%d-%dV", odrv.hw_version_major_, odrv.hw_version_minor_, odrv.hw_version_variant_);
    respond(use_checksum, "Firmware version: %d.%d.%d", odrv.fw_version_major_, odrv.fw_version_minor_, odrv.fw_version_revision_);
    respond(use_checksum, "Serial number: %s", serial_number_str);
}

// @brief Executes the system control command
// @param pStr buffer of ASCII encoded values
// @param response_channel reference to the stream to respond on
// @param use_checksum bool to indicate whether a checksum is required on response
void AsciiProtocol::cmd_system_ctrl(char * pStr, bool use_checksum) {
    switch (pStr[1])
    {
        case 's':   odrv.save_configuration();  break;  // Save config
        case 'e':   odrv.erase_configuration(); break;  // Erase config
        case 'r':   odrv.reboot();              break;  // Reboot
        case 'c':   odrv.clear_errors();        break;  // clear all errors and rearm brake resistor if necessary
        default:    /* default */               break;
    }
}

// @brief Executes the read parameter command
// @param pStr buffer of ASCII encoded values
// @param response_channel reference to the stream to respond on
// @param use_checksum bool to indicate whether a checksum is required on response
void AsciiProtocol::cmd_read_property(char * pStr, bool use_checksum) {
    char name[MAX_LINE_LENGTH];

    if (sscanf(pStr, "r %255s", name) < 1) {
        respond(use_checksum, "invalid command format");
    } else {
        Introspectable property = root_obj.get_child(name, sizeof(name));
        const StringConvertibleTypeInfo* type_info = dynamic_cast<const StringConvertibleTypeInfo*>(property.get_type_info());
        if (!type_info) {
            respond(use_checksum, "invalid property");
        } else {
            char response[10];
            bool success = type_info->get_string(property, response, sizeof(response));
            respond(use_checksum, success ? response : "not implemented");
        }
    }
}

// @brief Executes the set write position command
// @param pStr buffer of ASCII encoded values
// @param response_channel reference to the stream to respond on
// @param use_checksum bool to indicate whether a checksum is required on response
void AsciiProtocol::cmd_write_property(char * pStr, bool use_checksum) {
    char name[MAX_LINE_LENGTH];
    char value[MAX_LINE_LENGTH];

    if (sscanf(pStr, "w %255s %255s", name, value) < 1) {
        respond(use_checksum, "invalid command format");
    } else {
        Introspectable property = root_obj.get_child(name, sizeof(name));
        const StringConvertibleTypeInfo* type_info = dynamic_cast<const StringConvertibleTypeInfo*>(property.get_type_info());
        if (!type_info) {
            respond(use_checksum, "invalid property");
        } else {
            bool success = type_info->set_string(property, value, sizeof(value));
            if (!success) {
                respond(use_checksum, "not implemented");
            }
        }
    }
}

// @brief Executes the motor watchdog update command
// @param pStr buffer of ASCII encoded values
// @param response_channel reference to the stream to respond on
// @param use_checksum bool to indicate whether a checksum is required on response
void AsciiProtocol::cmd_update_axis_wdg(char * pStr, bool use_checksum) {
    unsigned motor_number;

    if (sscanf(pStr, "u %u", &motor_number) < 1) {
        respond(use_checksum, "invalid command format");
    } else if (motor_number >= AXIS_COUNT) {
        respond(use_checksum, "invalid motor %u", motor_number);
    } else {
        axes[motor_number].watchdog_feed();
    }
}

// @brief This is the implementation for the communication with the torque sensors
// @param pStr buffer of ASCII encoded values
// @param use_checksum bool to indicate whether a checksum is required on response
void AsciiProtocol::cmd_torque_sensor(char * pStr, bool use_checksum) {
    float torque = 0.0f;

    if (sscanf(pStr, "%f", &torque) < 1){
        respond(use_checksum, "invalid command format");
    }
    else{
        if(pStr[6] == '.'){
            axes[0].torque_sensor_.raw_moment = torque;
        }
        else if(pStr[7] == '.'){
            axes[1].torque_sensor_.raw_moment = torque;
        }

    }
}

// @brief Sends the unknown command response
// @param pStr buffer of ASCII encoded values
// @param response_channel reference to the stream to respond on
// @param use_checksum bool to indicate whether a checksum is required on response
void AsciiProtocol::cmd_unknown(char * pStr, bool use_checksum) {
    (void)pStr;
    respond(use_checksum, "unknown command");
}



void AsciiProtocol::on_write_finished(WriteResult result) {
    tx_handle_ = 0;

    if (result.status == kStreamOk && result.end < tx_end_) {
        // Not everything was written. Try again.
        tx_channel_->start_write({result.end, tx_end_}, &tx_handle_, MEMBER_CB(this, on_write_finished));
        return;
    }

    if (rx_end_) {
        uint8_t* rx_end = rx_end_;
        rx_end_ = nullptr;
        on_read_finished({kStreamOk, rx_end});
    }
}

void AsciiProtocol::on_read_finished(ReadResult result) {
    if (result.status != kStreamOk) {
        return;
    }

    for (;;) {
        uint8_t* end_of_line = std::find_if(rx_buf_, result.end, [](uint8_t c) {
            return c == '\r' || c == '\n' || c == '!';
        });

        if (end_of_line >= result.end) {
            break;
        }

        if (read_active_) {
            if (tx_handle_) {
                // TX is busy - inhibit processing of the incoming data until
                // on_write_finished() is invoked.
                rx_end_ = result.end;
                return;
            }

            process_line({rx_buf_, end_of_line});
        } else {
            // Ignoring this line cause it didn't start at a new-line character
            read_active_ = true;
        }
        
        // Discard the processed bytes and shift the remainder to the beginning of the buffer
        size_t n_remaining = result.end - end_of_line - 1;
        memmove(rx_buf_, end_of_line + 1, n_remaining);
        result.end = rx_buf_ + n_remaining;
    }

    // No more new-line characters in buffer

    if (result.end >= rx_buf_ + sizeof(rx_buf_)) {
        // If the line becomes too long, reset buffer and wait for the next line
        result.end = rx_buf_;
        read_active_ = false;
    }

    TransferHandle dummy;
    rx_channel_->start_read({result.end, rx_buf_ + sizeof(rx_buf_)}, &dummy, MEMBER_CB(this, on_read_finished));
}

void AsciiProtocol::start() {
    TransferHandle dummy;
    rx_channel_->start_read(rx_buf_, &dummy, MEMBER_CB(this, on_read_finished));
}
