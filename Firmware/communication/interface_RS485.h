#ifndef __INTERFACE_RS485_HPP
#define __INTERFACE_RS485_HPP


#ifdef __cplusplus
extern "C" {
#endif

#include <cmsis_os.h>
#include <Drivers/STM32/stm32_gpio.hpp>
#include "usart.h"

extern osThreadId RS485_thread;
extern const uint32_t stack_size_RS485_thread;

extern Stm32Gpio togglePin;

void start_RS485_server(UART_HandleTypeDef* huart);
void RS485_poll(void);
void RS485_write(void);
void RS485_TxCpltCallback(UART_HandleTypeDef* huart);

#ifdef __cplusplus
}
#endif


#ifdef __cplusplus
#include <fibre/../../stream_utils.hpp>
extern fibre::BufferedStreamSink<64> RS485_stdout_sink;
extern bool RS485_stdout_pending;
#endif

#endif // __INTERFACE_RS485_HPP
