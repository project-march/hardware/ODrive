
#include "interface_RS485.h"

#include "ascii_protocol.hpp"

#include <MotorControl/utils.hpp>

#include <fibre/async_stream.hpp>
#include <fibre/../../legacy_protocol.hpp>
#include <usart.h>
#include <cmsis_os.h>
#include <freertos_vars.h>
#include <odrive_main.h>

#define RS485_TX_BUFFER_SIZE 64
#define RS485_RX_BUFFER_SIZE 64

// DMA open loop continous circular buffer
// 1ms delay periodic, chase DMA ptr around
static uint8_t dma_rx_buffer[RS485_RX_BUFFER_SIZE];
static uint32_t dma_last_rcv_idx;

osThreadId RS485_thread = 0;
static UART_HandleTypeDef* huart_ = nullptr;
Stm32Gpio togglePin;
const uint32_t stack_size_RS485_thread = 4096;  // Bytes

namespace fibre {

class Stm32RS485TxStream : public AsyncStreamSink {
public:
    Stm32RS485TxStream(UART_HandleTypeDef* huart, Stm32Gpio* togglePin) : huart_(huart), DE(togglePin) {}

    void start_write(cbufptr_t buffer, TransferHandle* handle, Callback<void, WriteResult> completer) final;
    void cancel_write(TransferHandle transfer_handle) final;
    void did_finish();

    Stm32Gpio *DE;
    UART_HandleTypeDef *huart_;
    Callback<void, WriteResult> completer_;
    const uint8_t* tx_end_ = nullptr;
};

class Stm32RS485RxStream : public AsyncStreamSource {
public:
    void start_read(bufptr_t buffer, TransferHandle* handle, Callback<void, ReadResult> completer) final;
    void cancel_read(TransferHandle transfer_handle) final;
    void did_receive(uint8_t* buffer, size_t length);

    Callback<void, ReadResult> completer_;
    bufptr_t rx_buf_ = {nullptr, nullptr};
};

}

using namespace fibre;

void Stm32RS485TxStream::start_write(cbufptr_t buffer, TransferHandle* handle, Callback<void, WriteResult> completer) {
    size_t chunk = std::min(buffer.size(), (size_t)RS485_TX_BUFFER_SIZE);

    completer_ = completer;
    tx_end_ = buffer.begin() + chunk;

    if (handle) {
        *handle = reinterpret_cast<TransferHandle>(this);
    }

    //set RS485 driver control pin high before transmitting data
    DE->write(GPIO_PinState::GPIO_PIN_SET);

    if (HAL_UART_Transmit_DMA(huart_, const_cast<uint8_t*>(buffer.begin()), chunk) != HAL_OK) {
        completer_ = nullptr;
        tx_end_ = nullptr;
        completer.invoke({kStreamError, buffer.begin()});
    }
}

void Stm32RS485TxStream::cancel_write(TransferHandle transfer_handle) {
    // not implemented
}

void Stm32RS485TxStream::did_finish() {
    const uint8_t* tx_end = tx_end_;
    tx_end_ = nullptr;
    completer_.invoke_and_clear({kStreamOk, tx_end});
    
    //set RS485 driver control pin low after transmitting data
    DE->write(GPIO_PinState::GPIO_PIN_RESET);
}

void Stm32RS485RxStream::start_read(bufptr_t buffer, TransferHandle* handle, Callback<void, ReadResult> completer) {
    completer_ = completer;
    rx_buf_ = buffer;
    if (handle) {
        *handle = reinterpret_cast<TransferHandle>(this);
    }
}

void Stm32RS485RxStream::cancel_read(TransferHandle transfer_handle) {
    // not implemented
}

void Stm32RS485RxStream::did_receive(uint8_t* buffer, size_t length) {
    // This can be called even if there was no RX operation in progress

    bufptr_t rx_buf = rx_buf_;

    if (completer_ && rx_buf.begin()) {
        rx_buf_ = {nullptr, nullptr};
        size_t chunk = std::min(length, rx_buf.size());
        memcpy(rx_buf.begin(), buffer, chunk);
        completer_.invoke_and_clear({kStreamOk, rx_buf.begin() + chunk});
    }
}

Stm32RS485TxStream RS485_tx_stream(huart_, &togglePin);
Stm32RS485RxStream RS485_rx_stream;

LegacyProtocolStreamBased fibre_over_RS485(&RS485_rx_stream, &RS485_tx_stream);

fibre::AsyncStreamSinkMultiplexer<2> RS485_tx_multiplexer(RS485_tx_stream);
fibre::BufferedStreamSink<64> RS485_stdout_sink(RS485_tx_multiplexer); // Used in communication.cpp
AsciiProtocol ascii_over_RS485(&RS485_rx_stream, &RS485_tx_multiplexer);

bool RS485_stdout_pending = false;

static void RS485_server_thread(void * ctx) {
    (void) ctx;

    if (odrv.config_.uart1_protocol == ODrive::STREAM_PROTOCOL_TYPE_FIBRE) {
        fibre_over_RS485.start({});
    } else if (odrv.config_.uart1_protocol == ODrive::STREAM_PROTOCOL_TYPE_ASCII
            || odrv.config_.uart1_protocol == ODrive::STREAM_PROTOCOL_TYPE_ASCII_AND_STDOUT) {
        ascii_over_RS485.start();
    }

    for (;;) {
        osEvent event = osMessageGet(RS485_event_queue, osWaitForever);

        if (event.status != osEventMessage) {
            continue;
        }

        switch (event.value.v) {
            case 1: {
                // This event is triggered by the control loop at 8kHz. This should be
                // enough for most applications.
                // At 1Mbaud/s that corresponds to at most 12.5 bytes which can arrive
                // during the sleep period.

                // Check for UART errors and restart receive DMA transfer if required
                if (huart_->RxState != HAL_UART_STATE_BUSY_RX) {
                    HAL_UART_AbortReceive(huart_);
                    HAL_UART_Receive_DMA(huart_, dma_rx_buffer, sizeof(dma_rx_buffer));
                    dma_last_rcv_idx = 0;
                }
                // Fetch the circular buffer "write pointer", where it would write next
                uint32_t new_rcv_idx = RS485_RX_BUFFER_SIZE - huart_->hdmarx->Instance->NDTR;
                if (new_rcv_idx > RS485_RX_BUFFER_SIZE) { // defensive programming
                    continue;
                }

                // Process bytes in one or two chunks (two in case there was a wrap)
                if (new_rcv_idx < dma_last_rcv_idx) {
                    RS485_rx_stream.did_receive(dma_rx_buffer + dma_last_rcv_idx,
                            RS485_RX_BUFFER_SIZE - dma_last_rcv_idx);
                    dma_last_rcv_idx = 0;
                }
                if (new_rcv_idx > dma_last_rcv_idx) {
                    RS485_rx_stream.did_receive(dma_rx_buffer + dma_last_rcv_idx,
                            new_rcv_idx - dma_last_rcv_idx);
                    dma_last_rcv_idx = new_rcv_idx;
                }
            } break;

            case 2: {
                RS485_tx_stream.did_finish();
            } break;

            case 3: { // stdout has data
                RS485_stdout_pending = false;
                RS485_stdout_sink.maybe_start_async_write();
            } break;
        }
    }
}

// TODO: allow multiple UART server instances
void start_RS485_server(UART_HandleTypeDef* huart) {
    huart_ = huart;
    RS485_tx_stream.huart_ = huart;

    // DMA is set up to receive in a circular buffer forever.
    // We dont use interrupts to fetch the data, instead we periodically read
    // data out of the circular buffer into a parse buffer, controlled by a state machine
    HAL_UART_Receive_DMA(huart_, dma_rx_buffer, sizeof(dma_rx_buffer));
    dma_last_rcv_idx = 0;

    // Start UART communication thread
    osThreadDef(RS485_server_thread_def, RS485_server_thread, osPriorityAboveNormal, 0, stack_size_RS485_thread / sizeof(StackType_t) /* the ascii protocol needs considerable stack space */);
    RS485_thread = osThreadCreate(osThread(RS485_server_thread_def), NULL);
}

void RS485_poll() {
    if (RS485_thread) { // the thread is only started if UART is enabled
        osMessagePut(RS485_event_queue, 1, 0);
    }
}

void RS485_write() {
    if (RS485_thread) { // the thread is only started if UART is enabled
        osMessagePut(RS485_event_queue, 3, 0);
    }
}

void RS485_TxCpltCallback(UART_HandleTypeDef* huart) {
    if (huart == huart_) {
        //maybe switch toggle pin here
        osMessagePut(RS485_event_queue, 2, 0);
    }
}
