#include <odrive_main.h>
#include <Drivers/STM32/stm32_system.h>
#include <math.h>
#include <bitset>
#include <crc.hpp>
#include <torque_sensor.hpp>

TorqueSensor::TorqueSensor(){}

bool TorqueSensor::apply_config(ODriveIntf::MotorIntf::MotorType motor_type) {
    config_.parent = this;
    // update_pll_gains();
    if (config_.pre_calibrated) {
        if (config_.mode == TorqueSensor::MODE_MARCH_DCELL){
        is_ready_ = true;
        }
    }
    return true;
}


void TorqueSensor::setup() {
    //TODO: Torque sensor re-create following method
    mode_ = config_.mode;
}

void TorqueSensor::set_error(Error error) {
    moment_estimate_valid_ = false;
    error_ |= error;
}

bool TorqueSensor::do_checks(){
    return error_ == ERROR_NONE;
}

void TorqueSensor::check_pre_calibrated() {
    // TODO: restoring config from python backup is fragile here (ACIM motor type must be set first)
    if (axis_->motor_.config_.motor_type != Motor::MOTOR_TYPE_ACIM) {
        if (!is_ready_)
            config_.pre_calibrated = false;
    }
}

bool TorqueSensor::update() {

    // multiply with arm rotation - equals 1 for rotational joints
    converted_moment = raw_moment * config_.arm_rotation;

    // check if the moment is not too much
    if (converted_moment > config_.max_moment || converted_moment < (-1*config_.max_moment)){
        // set_error(ERROR_TORQUE_LIMIT_EXCEEDED);
    } 

    // set the moment estimate output from the torque sensor to the controller
    if(axis_->axis_num_ == 0){ 
        moment_estimate_ = config_.torque_orientation*converted_moment;      
        } else{
        moment_estimate_ = converted_moment;
        }
    // moment_estimate_float = 0.12f; //converted_moment;




}
