#ifndef __CONTROL_POSITION_HPP
#define __CONTROL_POSITION_HPP

class Controller;

class PositionControlloop{
public:
    
    float update(Controller* controller_);
};

#endif // __CONTROL_POSITION_HPP
