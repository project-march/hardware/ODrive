#ifndef __TORQUE_SENSOR_HPP
#define __TORQUE_SENSOR_HPP

class TorqueSensor;

#include <board.h> // needed for arm_math.h
#include <Drivers/STM32/stm32_spi_arbiter.hpp>
#include "utils.hpp"
#include <autogen/interfaces.hpp>
#include "component.hpp"

class TorqueSensor : public ODriveIntf::TorqueSensorIntf {
public:

    struct Config_t {
        bool pre_calibrated = false; // Overwritten by Odrivepython
        float arm_rotation = 1.0f; // Overwritten by Odrivepython for linears
        float max_moment = 1.0f;
        int32_t torque_orientation = 0;
        Mode mode = MODE_MARCH_DCELL; // Overwritten by Odrivepython
        // float calib_range = 0.02;
        // float calib_scan_distance = 16.0;
        // float calib_scan_omega = 4.0;
        float bandwidth = 1000.0;

        // custom setters
        TorqueSensor* parent = nullptr;
        void set_pre_calibrated(bool value) { pre_calibrated = value; parent->check_pre_calibrated(); }
    };

    Config_t config_;

    TorqueSensor();
    
    bool apply_config(ODriveIntf::MotorIntf::MotorType motor_type);
    void setup();
    void set_error(Error error);
    bool do_checks();

    void check_pre_calibrated();
    bool update();

    Axis* axis_ = nullptr; // set by Axis constructor

    Mode mode_ = MODE_MARCH_DCELL;
    Error error_ = ERROR_NONE;
    bool is_ready_ = false;

    // Set only by the custom march command
    // Temporary until torque sensors can be connected to the ODrive directly
    float raw_moment = 0.0f; 
    
    OutputPort<float> moment_estimate_ = 0.0f;
    float moment_estimate_float = 0.0f;
    float converted_moment = 0.0f;

    bool moment_estimate_valid_ = false;
};

#endif /* __TORQUE_SENSOR_HPP */
