#include <crc.hpp>
#include <stdint.h>

// CRC 0x97 polynomial, 64-bit input data, right alignment calculation over 64 bits
uint8_t CRC_SPI_97_64bit(uint64_t dw_InputData){
    uint8_t b_Index = 0;
    uint8_t b_CRC = 0;
    b_Index = (uint8_t)((dw_InputData >> 56u) & (uint64_t)0x000000FFu);
    b_CRC = (uint8_t)((dw_InputData >> 48u) & (uint64_t)0x000000FFu);
    b_Index = b_CRC ^ ab_CRC8_LUT[b_Index];
    b_CRC = (uint8_t)((dw_InputData >> 40u) & (uint64_t)0x000000FFu);
    b_Index = b_CRC ^ ab_CRC8_LUT[b_Index];
    b_CRC = (uint8_t)((dw_InputData >> 32u) & (uint64_t)0x000000FFu);
    b_Index = b_CRC ^ ab_CRC8_LUT[b_Index];
    b_CRC = (uint8_t)((dw_InputData >> 24u) & (uint64_t)0x000000FFu);
    b_Index = b_CRC ^ ab_CRC8_LUT[b_Index];
    b_CRC = (uint8_t)((dw_InputData >> 16u) & (uint64_t)0x000000FFu);
    b_Index = b_CRC ^ ab_CRC8_LUT[b_Index];
    b_CRC = (uint8_t)((dw_InputData >> 8u) & (uint64_t)0x000000FFu);
    b_Index = b_CRC ^ ab_CRC8_LUT[b_Index];
    b_CRC = (uint8_t)(dw_InputData & (uint64_t)0x000000FFu);
    b_Index = b_CRC ^ ab_CRC8_LUT[b_Index];
    b_CRC = ab_CRC8_LUT[b_Index];
    return b_CRC;
}

// CRC 0x97 polynomial, 32-bit input data, right alignment calculation over 32 bits
uint8_t CRC_SPI_97_32bit(uint32_t dw_InputData){
    uint8_t b_Index = 0;
    uint8_t b_CRC = 0;
    b_Index = (uint8_t)((dw_InputData >> 24u) & (uint32_t)0x000000FFu);
    b_CRC = (uint8_t)((dw_InputData >> 16u) & (uint32_t)0x000000FFu);
    b_Index = b_CRC ^ ab_CRC8_LUT[b_Index];
    b_CRC = (uint8_t)((dw_InputData >> 8u) & (uint32_t)0x000000FFu);
    b_Index = b_CRC ^ ab_CRC8_LUT[b_Index];
    b_CRC = (uint8_t)(dw_InputData & (uint32_t)0x000000FFu);
    b_Index = b_CRC ^ ab_CRC8_LUT[b_Index];
    b_CRC = ab_CRC8_LUT[b_Index];
    return b_CRC;
}

// CRC 0x97 polynomial, 16-bit input data, right alignment calculation over 16 bits
uint8_t CRC_SPI_97_16bit(uint16_t dw_InputData){
    uint8_t b_Index = 0;
    uint8_t b_CRC = 0;
    b_Index = (uint8_t)((dw_InputData >> 8u) & (uint16_t)0x00FFu);
    b_CRC = (uint8_t)(dw_InputData & (uint16_t)0x00FFu);
    b_Index = b_CRC ^ ab_CRC8_LUT[b_Index];
    b_CRC = ab_CRC8_LUT[b_Index];
    return b_CRC;
}
