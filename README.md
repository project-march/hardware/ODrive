<div align="center">
    <h1>
        <img src="https://gitlab.com/project-march/march/-/raw/main/.gitlab/readme/logo.png?inline=false" alt="Project MARCH" width=50%/>
    </h1>
</div>


## About

This repository contains a fork from the [ODrive](https://github.com/odriverobotics/ODrive) firmware. This fork has been adapted to the needs of the MARCH exoskeleton. The MARCH specific changes can be found in the [changelog](CHANGELOG.md).

<br>

![ODrive Logo](https://static1.squarespace.com/static/58aff26de4fcb53b5efd2f02/t/59bf2a7959cc6872bd68be7e/1505700483663/Odrive+logo+plus+text+black.png?format=1000w)

This project is all about accurately driving brushless motors, for cheap. The aim is to make it possible to use inexpensive brushless motors in high performance robotics projects, like [this](https://www.youtube.com/watch?v=WT4E5nb3KtY).


Please refer to the [Developer Guide](https://docs.odriverobotics.com/developer-guide) to get started with ODrive firmware development.


### Repository Structure
* **Firmware**: ODrive firmware
* **tools**: Python library & tools
* **docs**: Documentation

### Other Resources

* [Main Website](https://www.odriverobotics.com/)
* [User Guide](https://docs.odriverobotics.com/)
* [Forum](https://discourse.odriverobotics.com/)
* [Chat](https://discourse.odriverobotics.com/t/come-chat-with-us/281)

